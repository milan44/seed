package seed

import (
	"testing"
)

func TestParse(t *testing.T) {
	seed := "example seed"
	s := Parse(seed)

	if s.Int64 != 6398 {
		t.Fatalf("s.Int64 != 6398 -> %d", s.Int64)
	}

	if s.Format(36) != "4xq" {
		t.Fatalf("s.Format(36) != 4xq -> %s", s.Format(36))
	}
}
