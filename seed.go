package seed

import (
	"math/big"
	"math/rand"
	"strconv"
)

type Seed struct {
	Int64  int64
	String string
}

func (s Seed) Format(base int) string {
	return strconv.FormatInt(s.Int64, base)
}

func Parse(s string) Seed {
	result := big.NewInt(0)
	runes := []rune(s)

	for x, r := range runes {
		i := big.NewInt(int64(r))
		i = big.NewInt(0).Mul(i, big.NewInt(int64(x)))

		result = big.NewInt(0).Add(result, i)
	}

	return Seed{
		Int64:  result.Int64(),
		String: s,
	}
}

func Rand(seed string) *rand.Rand {
	src := rand.NewSource(Parse(seed).Int64)
	return rand.New(src)
}
