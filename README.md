# Seed
Small library for parsing strings to usable int64 seeds.

### Usage

```golang
package main

import (
    "fmt"
    "gitlab.com/milan44/seed"
)

func main() {
    s := seed.Parse("example seed")

    fmt.Println(s.Int64) // 6398
}
```